package com.mrpenguin.transmuteliquids.lib;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.item.ItemStack;
import thaumcraft.api.ThaumcraftApi;
import thaumcraft.api.aspects.Aspect;
import thaumcraft.api.aspects.AspectList;
import thaumcraft.api.crafting.CrucibleRecipe;
import thaumcraft.api.research.ResearchPage;

/**
 * Copyright (c) 2014 MrPenguin
 * All rights reserved.
 *
 * This program and the accompanying materials are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 *
 * This class was made by MrPenguin and is distributed as a part of the Transmute Liquids mod.
 * Transmute Liquids is a derivative work on Thaumcraft 4 (c) Azanor 2012.
 * http://www.minecraftforum.net/topic/2011841-thaumcraft-41114-updated-2052014/
 */

public class Railcraft {
    public static final String Railcraft = "Railcraft";

    public static final String transmuteCreosoteOil = "transmuteCreosoteOil";

    public static final String transmuteCreosoteOilText = "transmuteCreosoteOilText";

    public static final ItemStack bottleCreosoteOil = GameRegistry.findItemStack(Railcraft, "fluid.creosote.bottle", 1);
    public static final ItemStack bucketCreosoteOil = GameRegistry.findItemStack(Railcraft, "fluid.creosote.bucket", 1);
    public static final ItemStack waxCapsuleCreosoteOil = GameRegistry.findItemStack(Railcraft, "fluid.creosote.wax", 1);
    public static final ItemStack refractoryCreosoteOil = GameRegistry.findItemStack(Railcraft, "fluid.creosote.refactory", 1);
    public static final ItemStack canCreosoteOil = GameRegistry.findItemStack(Railcraft, "fluid.creosote.can", 1);

    public static final AspectList creosoteOilAspectList1 = new AspectList().add(Aspect.FIRE, 8).add(Aspect.ENERGY, 8).add(Aspect.TOOL, 3).add(Aspect.EXCHANGE, 3);
    public static final AspectList creosoteOilAspectList2 = new AspectList().add(Aspect.FIRE, 4).add(Aspect.ENERGY, 4).add(Aspect.TOOL, 1).add(Aspect.EXCHANGE, 3);

    public static final CrucibleRecipe bottleCreosoteOilRecipe = ThaumcraftApi.addCrucibleRecipe(transmuteCreosoteOil, bottleCreosoteOil, TransmuteLiquids.glassBottle, creosoteOilAspectList2);
    public static final CrucibleRecipe bucketCreosoteOilRecipe = ThaumcraftApi.addCrucibleRecipe(transmuteCreosoteOil, bucketCreosoteOil, TransmuteLiquids.bucket, creosoteOilAspectList1);
    public static final CrucibleRecipe waxCapsuleCreosoteOilRecipe = ThaumcraftApi.addCrucibleRecipe(transmuteCreosoteOil, waxCapsuleCreosoteOil, Forestry.waxCapsule, creosoteOilAspectList1);
    public static final CrucibleRecipe refractoryCreosoteOilRecipe = ThaumcraftApi.addCrucibleRecipe(transmuteCreosoteOil, refractoryCreosoteOil, Forestry.refractoryEmpty, creosoteOilAspectList1);
    public static final CrucibleRecipe canCreosoteOilRecipe = ThaumcraftApi.addCrucibleRecipe(transmuteCreosoteOil, canCreosoteOil, Forestry.canEmpty, creosoteOilAspectList1);

    public static final ResearchPage transmuteCreosoteOilTextPage = new ResearchPage(transmuteCreosoteOilText);
    public static final ResearchPage bottleCreosoteOilRecipePage = new ResearchPage(bottleCreosoteOilRecipe);
    public static final ResearchPage bucketCreosoteOilRecipePage = new ResearchPage(bucketCreosoteOilRecipe);
    public static final ResearchPage waxCapsuleCreosoteOilRecipePage = new ResearchPage(waxCapsuleCreosoteOilRecipe);
    public static final ResearchPage refractoryCreosoteOilRecipePage = new ResearchPage(refractoryCreosoteOilRecipe);
    public static final ResearchPage canCreosoteOilRecipePage = new ResearchPage(canCreosoteOilRecipe);
    public static final ResearchPage[] transmuteCreosoteOilPages = {transmuteCreosoteOilTextPage, bottleCreosoteOilRecipePage, bucketCreosoteOilRecipePage};
    public static final ResearchPage[] transmuteCreosoteOilPagesForestry = {transmuteCreosoteOilTextPage, bottleCreosoteOilRecipePage, bucketCreosoteOilRecipePage, waxCapsuleCreosoteOilRecipePage, refractoryCreosoteOilRecipePage, canCreosoteOilRecipePage};
}
