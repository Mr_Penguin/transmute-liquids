package com.mrpenguin.transmuteliquids.lib;

/**
 * Copyright (c) 2014 MrPenguin
 * All rights reserved.
 *
 * This program and the accompanying materials are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 *
 * This class was made by MrPenguin and is distributed as a part of the Transmute Liquids mod.
 * Transmute Liquids is a derivative work on Thaumcraft 4 (c) Azanor 2012.
 * http://www.minecraftforum.net/topic/2011841-thaumcraft-41114-updated-2052014/
 */

import com.mrpenguin.transmuteliquids.items.ModItems;
import thaumcraft.api.ItemApi;
import thaumcraft.api.ThaumcraftApi;
import thaumcraft.api.aspects.Aspect;
import thaumcraft.api.aspects.AspectList;
import thaumcraft.api.crafting.CrucibleRecipe;
import thaumcraft.api.crafting.InfusionRecipe;
import thaumcraft.api.research.ResearchPage;

import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;

public class TransmuteLiquids {

    public static final String transmuteLiquids = "transmuteLiquids";

    public static final String Thaumcraft = "Thaumcraft";

    public static final String transmuteIron = "TRANSIRON";
    public static final String transmuteWater = "transmuteWater";
    public static final String transmuteLava = "transmuteLava";

    public static final String infuseMoltenShardBuckets = "infuseMoltenShardBuckets";

    public static final String transmuteShardIngot = "transmuteShardIngot";

    public static final String transmuteWaterText = "transmuteWaterText";
    public static final String transmuteLavaText = "transmuteLavaText";

    public static final String infuseMoltenShardBucketText = "infuseMoltenShardBucketText";

    public static final String transmuteShardIngotText = "transmuteShardIngotText";

    public static final ItemStack bucket = new ItemStack(Items.bucket);
    public static final ItemStack glassBottle = new ItemStack(Items.glass_bottle);

    public static final ItemStack waterBucket = new ItemStack(Items.water_bucket);
    public static final ItemStack lavaBucket = new ItemStack(Items.lava_bucket);

    public static final ItemStack airShard = ItemApi.getItem("itemShard", 0);
    public static final ItemStack fireShard = ItemApi.getItem("itemShard", 1);
    public static final ItemStack waterShard = ItemApi.getItem("itemShard", 2);
    public static final ItemStack earthShard = ItemApi.getItem("itemShard", 3);
    public static final ItemStack orderShard = ItemApi.getItem("itemShard", 4);
    public static final ItemStack entropyShard = ItemApi.getItem("itemShard", 5);

    public static final ItemStack[] infuseMoltenAirShardBucketItemStack = {airShard, airShard, airShard};
    public static final ItemStack[] infuseMoltenFireShardBucketItemStack = {fireShard, fireShard, fireShard};
    public static final ItemStack[] infuseMoltenWaterShardBucketItemStack = {waterShard, waterShard, waterShard};
    public static final ItemStack[] infuseMoltenEarthShardBucketItemStack = {earthShard, earthShard, earthShard};
    public static final ItemStack[] infuseMoltenOrderShardBucketItemStack = {orderShard, orderShard, orderShard};
    public static final ItemStack[] infuseMoltenEntropyShardBucketItemStack = {entropyShard, entropyShard, entropyShard};

    public static final ItemStack moltenAirShardBucket = new ItemStack(ModItems.moltenAirShardBucket, 1, 0);
    public static final ItemStack moltenFireShardBucket = new ItemStack(ModItems.moltenFireShardBucket, 1, 0);
    public static final ItemStack moltenWaterShardBucket = new ItemStack(ModItems.moltenEarthShardBucket, 1, 0);
    public static final ItemStack moltenEarthShardBucket = new ItemStack(ModItems.moltenEarthShardBucket, 1, 0);
    public static final ItemStack moltenOrderShardBucket = new ItemStack(ModItems.moltenOrderShardBucket, 1, 0);
    public static final ItemStack moltenEntropyShardBucket = new ItemStack(ModItems.moltenEntropyShardBucket, 1, 0);

    public static final ItemStack airIngot = new ItemStack(ModItems.airIngot);
    public static final ItemStack fireIngot = new ItemStack(ModItems.fireIngot);
    public static final ItemStack waterIngot = new ItemStack(ModItems.waterIngot);
    public static final ItemStack earthIngot = new ItemStack(ModItems.earthIngot);
    public static final ItemStack orderIngot = new ItemStack(ModItems.orderIngot);
    public static final ItemStack entropyIngot = new ItemStack(ModItems.entropyIngot);

    public static final AspectList waterAspectList = new AspectList().add(Aspect.WATER, 6).add(Aspect.EXCHANGE, 3);
    public static final AspectList lavaAspectList = new AspectList().add(Aspect.FIRE, 6).add(Aspect.EXCHANGE, 3);

    public static final AspectList moltenAirShardBucketAspectList = new AspectList().add(Aspect.AIR, 10).add(Aspect.MAGIC, 5).add(Aspect.FIRE, 5);
    public static final AspectList moltenFireShardBucketAspectList = new AspectList().add(Aspect.FIRE, 15).add(Aspect.MAGIC, 5);
    public static final AspectList moltenWaterShardBucketAspectList = new AspectList().add(Aspect.WATER, 10).add(Aspect.MAGIC, 5).add(Aspect.FIRE, 5);
    public static final AspectList moltenEarthShardBucketAspectList = new AspectList().add(Aspect.EARTH, 10).add(Aspect.MAGIC, 5).add(Aspect.FIRE, 5);
    public static final AspectList moltenOrderShardBucketAspectList = new AspectList().add(Aspect.ORDER, 10).add(Aspect.MAGIC, 5).add(Aspect.FIRE, 5);
    public static final AspectList moltenEntropyShardBucketAspectList = new AspectList().add(Aspect.ENTROPY, 10).add(Aspect.MAGIC, 5).add(Aspect.FIRE, 5);

    public static final AspectList shardIngotAspectList = new AspectList().add(Aspect.METAL, 5).add(Aspect.MAGIC, 5);

    public static final CrucibleRecipe waterBucketRecipe = ThaumcraftApi.addCrucibleRecipe(transmuteWater, waterBucket, bucket, waterAspectList);
    public static final CrucibleRecipe lavaBucketRecipe = ThaumcraftApi.addCrucibleRecipe(transmuteLava, lavaBucket, bucket, lavaAspectList);

    public static final InfusionRecipe moltenAirShardBucketRecipe = ThaumcraftApi.addInfusionCraftingRecipe(infuseMoltenShardBuckets, moltenAirShardBucket, 2, moltenAirShardBucketAspectList, lavaBucket, infuseMoltenAirShardBucketItemStack);
    public static final InfusionRecipe moltenFireShardBucketRecipe = ThaumcraftApi.addInfusionCraftingRecipe(infuseMoltenShardBuckets, moltenFireShardBucket, 2, moltenFireShardBucketAspectList, lavaBucket, infuseMoltenFireShardBucketItemStack);
    public static final InfusionRecipe moltenWaterShardBucketRecipe = ThaumcraftApi.addInfusionCraftingRecipe(infuseMoltenShardBuckets, moltenWaterShardBucket, 2, moltenWaterShardBucketAspectList, lavaBucket, infuseMoltenWaterShardBucketItemStack);
    public static final InfusionRecipe moltenEarthShardBucketRecipe = ThaumcraftApi.addInfusionCraftingRecipe(infuseMoltenShardBuckets, moltenEarthShardBucket, 2, moltenEarthShardBucketAspectList, lavaBucket, infuseMoltenEarthShardBucketItemStack);
    public static final InfusionRecipe moltenOrderShardBucketRecipe = ThaumcraftApi.addInfusionCraftingRecipe(infuseMoltenShardBuckets, moltenOrderShardBucket, 2, moltenOrderShardBucketAspectList, lavaBucket, infuseMoltenOrderShardBucketItemStack);
    public static final InfusionRecipe moltenEntropyShardBucketRecipe = ThaumcraftApi.addInfusionCraftingRecipe(infuseMoltenShardBuckets, moltenEntropyShardBucket, 2, moltenEntropyShardBucketAspectList, lavaBucket, infuseMoltenEntropyShardBucketItemStack);

    public static final CrucibleRecipe airIngotRecipe = ThaumcraftApi.addCrucibleRecipe(transmuteShardIngot, airIngot, moltenAirShardBucket, shardIngotAspectList);
    public static final CrucibleRecipe fireIngotRecipe = ThaumcraftApi.addCrucibleRecipe(transmuteShardIngot, fireIngot, moltenFireShardBucket, shardIngotAspectList);
    public static final CrucibleRecipe waterIngotRecipe = ThaumcraftApi.addCrucibleRecipe(transmuteShardIngot, waterIngot, moltenWaterShardBucket, shardIngotAspectList);
    public static final CrucibleRecipe earthIngotRecipe = ThaumcraftApi.addCrucibleRecipe(transmuteShardIngot, earthIngot, moltenEarthShardBucket, shardIngotAspectList);
    public static final CrucibleRecipe orderIngotRecipe = ThaumcraftApi.addCrucibleRecipe(transmuteShardIngot, orderIngot, moltenOrderShardBucket, shardIngotAspectList);
    public static final CrucibleRecipe entropyIngotRecipe = ThaumcraftApi.addCrucibleRecipe(transmuteShardIngot, entropyIngot, moltenEntropyShardBucket, shardIngotAspectList);

    public static final ResearchPage waterTextPage = new ResearchPage(transmuteWaterText);
    public static final ResearchPage bucketWaterRecipePage = new ResearchPage(waterBucketRecipe);
    public static final ResearchPage[] transmuteWaterPages = {waterTextPage, bucketWaterRecipePage};

    public static final ResearchPage lavaTextPage = new ResearchPage(transmuteLavaText);
    public static final ResearchPage bucketLavaRecipePage = new ResearchPage(lavaBucketRecipe);
    public static final ResearchPage[] transmuteLavaPages = {lavaTextPage, bucketLavaRecipePage};

    public static final ResearchPage infuseMoltenShardBucketTextPage = new ResearchPage(infuseMoltenShardBucketText);
    public static final ResearchPage moltenAirShardBucketRecipePage = new ResearchPage(moltenAirShardBucketRecipe);
    public static final ResearchPage moltenFireShardBucketRecipePage = new ResearchPage(moltenFireShardBucketRecipe);
    public static final ResearchPage moltenWaterShardBucketRecipePage = new ResearchPage(moltenWaterShardBucketRecipe);
    public static final ResearchPage moltenEarthShardBucketRecipePage = new ResearchPage(moltenEarthShardBucketRecipe);
    public static final ResearchPage moltenOrderShardBucketRecipePage = new ResearchPage(moltenOrderShardBucketRecipe);
    public static final ResearchPage moltenEntropyShardBucketRecipePage = new ResearchPage(moltenEntropyShardBucketRecipe);
    public static final ResearchPage[] infuseMoltenShardBucketPages = {infuseMoltenShardBucketTextPage, moltenAirShardBucketRecipePage, moltenFireShardBucketRecipePage, moltenWaterShardBucketRecipePage, moltenEarthShardBucketRecipePage, moltenOrderShardBucketRecipePage, moltenEntropyShardBucketRecipePage};

    public static final ResearchPage transmuteShardIngotTextPage = new ResearchPage(transmuteShardIngotText);
    public static final ResearchPage airIngotRecipePage = new ResearchPage(airIngotRecipe);
    public static final ResearchPage fireIngotRecipePage = new ResearchPage(fireIngotRecipe);
    public static final ResearchPage waterIngotRecipePage = new ResearchPage(waterIngotRecipe);
    public static final ResearchPage earthIngotRecipePage = new ResearchPage(earthIngotRecipe);
    public static final ResearchPage orderIngotRecipePage = new ResearchPage(orderIngotRecipe);
    public static final ResearchPage entropyShardRecipePage = new ResearchPage(entropyIngotRecipe);
    public static final ResearchPage[] transmuteShardIngotPages = {transmuteShardIngotTextPage, airIngotRecipePage, fireIngotRecipePage, waterIngotRecipePage, earthIngotRecipePage, orderIngotRecipePage, entropyShardRecipePage};
}
