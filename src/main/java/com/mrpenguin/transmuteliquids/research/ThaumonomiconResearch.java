package com.mrpenguin.transmuteliquids.research;

/**
 * Copyright (c) 2014 MrPenguin
 * All rights reserved.
 *
 * This program and the accompanying materials are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 *
 * This class was made by MrPenguin and is distributed as a part of the Transmute Liquids mod.
 * Transmute Liquids is a derivative work on Thaumcraft 4 (c) Azanor 2012.
 * http://www.minecraftforum.net/topic/2011841-thaumcraft-41114-updated-2052014/
 */

import com.mrpenguin.transmuteliquids.items.ModItems;
import com.mrpenguin.transmuteliquids.lib.Buildcraft;
import com.mrpenguin.transmuteliquids.lib.Forestry;
import com.mrpenguin.transmuteliquids.lib.Railcraft;
import com.mrpenguin.transmuteliquids.lib.TransmuteLiquids;

import cpw.mods.fml.common.Loader;
import net.minecraft.block.BlockRailBase;
import thaumcraft.api.aspects.AspectList;
import thaumcraft.api.research.ResearchCategories;
import thaumcraft.api.research.ResearchItem;

import net.minecraft.util.ResourceLocation;

public class ThaumonomiconResearch {
    public static void addResearchTab() {
        ResourceLocation background = new ResourceLocation("transmuteliquids:textures/gui/thaumonomiconBackground.png");
        ResourceLocation icon = new ResourceLocation("transmuteliquids:textures/misc/transmuteLiquidsIcon.png");
        ResearchCategories.registerCategory(TransmuteLiquids.transmuteLiquids, icon, background);
    }

    public static void addResearch() {
        if(Loader.isModLoaded(Forestry.Forestry)) {
            addForestryResearch();
            System.out.println("[TL] Transmute Liquids : Successfully Loaded Forestry Research");
        }else{
            addVanillaResearch();
        }
        if(Loader.isModLoaded(Buildcraft.buildcraftEnergy)) {
            addBuildcraftResearch();
            System.out.println("[TL] Transmute Liquids : Successfully Loaded Buildcraft Research");
        }
        if(Loader.isModLoaded(Railcraft.Railcraft)) {
            addRailcraftResearch();
            System.out.println("[TL] Transmute Liquids : Successfully Loaded Railcraft Research");
        }

        addInfusionResearch();
    }

    public static void addVanillaResearch() {
        ResearchItem transmuteWater;
        ResearchItem transmuteLava;
        ResearchItem infuseMoltenShardBuckets;

        transmuteWater = new TLResearchItem(TransmuteLiquids.transmuteWater, TransmuteLiquids.transmuteLiquids, new AspectList(), 0, 0, 1, TransmuteLiquids.waterBucket)
                .setPages(TransmuteLiquids.transmuteWaterPages).setSpecial().setParents(TransmuteLiquids.transmuteIron)
                .registerResearchItem();

        transmuteLava = new TLResearchItem(TransmuteLiquids.transmuteLava, TransmuteLiquids.transmuteLiquids, new AspectList(), 0, 2, 2, TransmuteLiquids.lavaBucket)
                .setPages(TransmuteLiquids.transmuteLavaPages).setConcealed().setSecondary().setParents(TransmuteLiquids.transmuteWater)
                .registerResearchItem();
    }

    public static void addInfusionResearch() {
        ResearchItem infuseMoltenShardBuckets;

        infuseMoltenShardBuckets = new TLResearchItem(TransmuteLiquids.infuseMoltenShardBuckets, TransmuteLiquids.transmuteLiquids, new AspectList(), 0, -2, 3, TransmuteLiquids.moltenAirShardBucket)
                .setPages(TransmuteLiquids.infuseMoltenShardBucketPages).setConcealed().setSpecial().setParents(TransmuteLiquids.transmuteWater)
                .registerResearchItem();
    }

    public static void addCrucibleResearch() {
        ResearchItem transmuteShardIngot;

        transmuteShardIngot = new TLResearchItem(TransmuteLiquids.transmuteShardIngot, TransmuteLiquids.transmuteLiquids, new AspectList(), 0, -4, 3, TransmuteLiquids.airIngot)
                .setPages(TransmuteLiquids.transmuteShardIngotPages).setConcealed().setSpecial().setParents(TransmuteLiquids.infuseMoltenShardBuckets)
                .registerResearchItem();
    }

    public static void addForestryResearch() {
        ResearchItem transmuteWater;
        ResearchItem transmuteLava;
        ResearchItem transmuteHoney;
        ResearchItem transmuteJuice;
        ResearchItem transmuteSeedOil;
        ResearchItem transmuteBiomass;
        ResearchItem transmuteBiofuel;

        transmuteWater = new TLResearchItem(TransmuteLiquids.transmuteWater, TransmuteLiquids.transmuteLiquids, new AspectList(), 0, 0, 1, TransmuteLiquids.waterBucket)
                .setPages(Forestry.transmuteWaterPages).setSpecial().setParents(TransmuteLiquids.transmuteIron)
                .registerResearchItem();
        transmuteLava = new TLResearchItem(TransmuteLiquids.transmuteLava, TransmuteLiquids.transmuteLiquids, new AspectList(), 0, 2, 2, TransmuteLiquids.lavaBucket)
                .setPages(Forestry.transmuteLavaPages).setConcealed().setParents(TransmuteLiquids.transmuteWater)
                .registerResearchItem();
        transmuteHoney = new TLResearchItem(Forestry.transmuteHoney, TransmuteLiquids.transmuteLiquids, new AspectList(), -2, 2, 0, Forestry.canHoney)
                .setPages(Forestry.transmuteHoneyPages).setConcealed().setParents(TransmuteLiquids.transmuteWater)
                .registerResearchItem();
        transmuteJuice = new TLResearchItem(Forestry.transmuteJuice, TransmuteLiquids.transmuteLiquids, new AspectList(), -3, 4, 2, Forestry.canJuice)
                .setPages(Forestry.transmuteJuicePages).setConcealed().setParents(Forestry.transmuteHoney)
                .registerResearchItem();
        transmuteSeedOil = new TLResearchItem(Forestry.transmuteSeedOil, TransmuteLiquids.transmuteLiquids, new AspectList(), 2, -2, 2, Forestry.canSeedOil)
                .setPages(Forestry.transmuteSeedOilPages).setConcealed().setParents(TransmuteLiquids.transmuteWater)
                .registerResearchItem();
        transmuteBiomass = new TLResearchItem(Forestry.transmuteBiomass, TransmuteLiquids.transmuteLiquids, new AspectList(), 3, -4, 2, Forestry.canBiomass)
                .setPages(Forestry.transmuteBiomassPages).setConcealed().setParents(Forestry.transmuteSeedOil)
                .registerResearchItem();
        transmuteBiofuel = new TLResearchItem(Forestry.transmuteBiofuel, TransmuteLiquids.transmuteLiquids, new AspectList(), 4, -6, 3, Forestry.canBiofuel)
                .setPages(Forestry.transmuteBiofuelPages).setSpecial().setConcealed().setParents(Forestry.transmuteBiomass)
                .registerResearchItem();
    }

    public static void addBuildcraftResearch() {
        ResearchItem transmuteOil;
        ResearchItem transmuteFuel;

        if(Loader.isModLoaded(Forestry.Forestry)) {
            transmuteOil = new TLResearchItem(Buildcraft.transmuteOil, TransmuteLiquids.transmuteLiquids, new AspectList(), 2, 2, 2, Buildcraft.bucketOil)
                .setPages(Buildcraft.transmuteOilPagesForestry).setConcealed().setParents(TransmuteLiquids.transmuteWater)
                .registerResearchItem();
            transmuteFuel = new TLResearchItem(Buildcraft.transmuteFuel, TransmuteLiquids.transmuteLiquids, new AspectList(), 3, 4, 3, Buildcraft.bucketFuel)
                .setPages(Buildcraft.transmuteFuelPagesForestry).setSpecial().setConcealed().setParents(Buildcraft.transmuteOil)
                .registerResearchItem();
        }else{
            transmuteOil = new TLResearchItem(Buildcraft.transmuteOil, TransmuteLiquids.transmuteLiquids, new AspectList(), 2, 2, 2, Buildcraft.bucketOil)
                .setPages(Buildcraft.transmuteOilPages).setConcealed().setParents(TransmuteLiquids.transmuteWater)
                .registerResearchItem();
            transmuteFuel = new TLResearchItem(Buildcraft.transmuteFuel, TransmuteLiquids.transmuteLiquids, new AspectList(), 3, 4, 3, Buildcraft.bucketFuel)
                .setPages(Buildcraft.transmuteFuelPages).setSpecial().setConcealed().setParents(Buildcraft.transmuteOil)
                .registerResearchItem();
        }
    }

    public static void addRailcraftResearch() {
        ResearchItem transmuteCreosoteOil;

        if(Loader.isModLoaded(Forestry.Forestry)) {
            transmuteCreosoteOil = new TLResearchItem(Railcraft.transmuteCreosoteOil, TransmuteLiquids.transmuteLiquids, new AspectList(), 0, 2, 2, Railcraft.bottleCreosoteOil)
                .setPages(Railcraft.transmuteCreosoteOilPagesForestry).setConcealed().setParents(TransmuteLiquids.transmuteWater)
                .registerResearchItem();
        }else{
            transmuteCreosoteOil = new TLResearchItem(Railcraft.transmuteCreosoteOil, TransmuteLiquids.transmuteLiquids, new AspectList(), 0, 2, 2, Railcraft.bottleCreosoteOil)
                .setPages(Railcraft.transmuteCreosoteOilPages).setConcealed().setParents(TransmuteLiquids.transmuteWater)
                .registerResearchItem();
        }
    }
}
